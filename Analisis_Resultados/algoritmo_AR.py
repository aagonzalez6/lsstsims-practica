def ARtest(densidades,left,center,width):
    """ 
    densidades = (valor de cada bin) / (ancho temporal de cada bin). Lista.
    left = Tiempo correspondiente al extremo izquierdo de cada bin. lista.
    center = Tiempo correspondiente al tiempo promedio entre los 2 extremos de cada bin. Lista.
    width = Ancho temporal de cada bin. Lista.
    """
    limites_izq = left
    limiets_medio = center
    ancho_bines = width
    ultimo_limite_der = limites_izq[-1] + ancho_bines[-1]
    total_dias = ultimo_limite_der
    total_muestras = np.sum(np.array(densidades)*ancho_bines)
    densidad_esperada = total_muestras/total_dias
    densidades_esperadas = np.ones(len(densidades))*densidad_esperada 
    parejitas = list(zip(densidades_esperadas,densidades,ancho_bines))
    acumulados = []
    valor_autoseleccionR = 0
    for x in parejitas:
        esperado = x[0]
        obtenido = x[1]
        if obtenido < esperado:
            dif_norm = (esperado-obtenido)/esperado
            # print("difnorm:",dif_norm)
            castigo = 10**(dif_norm)

            if len(acumulados)>0:
                if acumulados[-1] == 1 :
                    castigo =2*castigo
            if dif_norm > 0.75:
                castigo = castigo*2
            valor_autoseleccionR += castigo
            acumulados.append(dif_norm)
        else:
            acumulados = []
    return valor_autoseleccionR
