import os 
import sys
from fnmatch import fnmatch


files_folder1 = []
files_folder2 = []
for x in [f for f in os.listdir("./FBS_1.5") if not os.path.isdir(f)]:
	files_folder1.append(x)

for x in [f for f in os.listdir("./FBS_1.4.1") if not os.path.isdir(f)]:
	files_folder2.append(x)	

files_folder1 = [(x,x.split("v1.")[0].rstrip("_")) for x in files_folder1]
files_folder2 = [(x,x.split("v1.")[0].rstrip("_")) for x in files_folder2]


""" SE VERIFICA CUALES ARCHIVOS DE FOLDER 2 NO ESTAN EN FOLDER 1"""
lista_limpios_folder1 = [x[1] for x in files_folder1]

move_here = "./1.4.1_notin_1.5/"
if not os.path.exists(os.path.abspath(move_here)):
        os.makedirs(os.path.abspath(move_here),exist_ok=True)

for filename in files_folder2:
	nombre_full = filename[0]
	nombre_limpio = filename[1]
	if nombre_limpio not in lista_limpios_folder1:
		print(nombre_limpio)
		os.replace("./FBS_1.4.1/"+nombre_full,move_here+nombre_full)
