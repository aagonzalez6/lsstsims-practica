from data import *
import numpy as np 
from lf_shape import *
import scipy.interpolate as inter
from scipy.integrate import quad
from scipy.integrate import romberg
from convolve import *
from scipy.optimize import curve_fit
from scipy.optimize import minimize
from scipy.optimize import least_squares
import astropy.constants as con
# fit the luminosity function based on datasets at a given redshift
from ctypes import *
import ctypes
import sys

#parameters of the H07 model
parameters_init = np.array([0.41698725, 2.17443860, -4.82506430, 13.03575300, 0.63150872, -11.76356000, -14.24983300, -0.62298947, 1.45993930, -0.79280099])

#our global best-fit
fit_evolve=np.genfromtxt("../../Fit_parameters/codes/zevolution_fit_global.dat",names=True)
paraid, pglobal, pglobal_err = fit_evolve['paraid'], fit_evolve['value'], (fit_evolve['uperr']+fit_evolve['loerr'])/2.

fit_evolve_shallowfaint=np.genfromtxt("../../Fit_parameters/codes/zevolution_fit_global_shallowfaint.dat",names=True)
paraid_shallowfaint, pglobal_shallowfaint, pglobal_err_shallowfaint = fit_evolve_shallowfaint['paraid'], fit_evolve_shallowfaint['value'], (fit_evolve_shallowfaint['uperr']+fit_evolve_shallowfaint['loerr'])/2.

#load the shared object file
c_extenstion = CDLL(homepath+'codes/c_lib/specialuse/convolve_for_CXB.so')
convolve_c = c_extenstion.convolve
convolve_c.restype = ctypes.POINTER(ctypes.c_double * N_bol_grid_for_CXB)
##############
c_extenstion1 = CDLL(homepath+'codes/c_lib/specialuse/CTK_convolve.so')   #only convolve CTK objects
convolve_c1 = c_extenstion1.convolve
convolve_c1.restype = ctypes.POINTER(ctypes.c_double * N_bol_grid_for_CXB)

c_extenstion2 = CDLL(homepath+'codes/c_lib/specialuse/CTNabs_convolve.so') #only convolve absorbed CTN objects
convolve_c2 = c_extenstion2.convolve
convolve_c2.restype = ctypes.POINTER(ctypes.c_double * N_bol_grid_for_CXB)

c_extenstion3 = CDLL(homepath+'codes/c_lib/specialuse/CTNunabs_convolve.so') #only convolve unabsorbed CTN objects
convolve_c3 = c_extenstion3.convolve
convolve_c3.restype = ctypes.POINTER(ctypes.c_double * N_bol_grid_for_CXB)
##############

convolver_list = (convolve_c, convolve_c1, convolve_c2, convolve_c3)

def get_model_lf(parameters,nu,redshift,dtg,convolver):
        L_band = bolometric_correction(L_bol_grid_for_CXB,nu)
        nu_c = c_double(nu)
	redshift_c = c_double(redshift)
	dtg_c = c_double(dtg)
        input_c= np.power(10.,LF(L_bol_grid_for_CXB,parameters)).ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        res = convolver(input_c,nu_c,redshift_c, dtg_c)
        res = [i for i in res.contents]
        PHI_band = np.array(res,dtype=np.float64)
	return L_band, np.log10(PHI_band)

def get_model_lf_global(nu,redshift,dtg,convolver,model="Fiducial"):
	if model=="Fiducial":
		parameters=pglobal
		zref = 2.
		p=parameters[paraid==0]
		gamma1 = polynomial(redshift,p,2)
		p=parameters[paraid==1]
		gamma2 = doublepower(redshift,(p[0],zref, p[1], p[2]))
		p=parameters[paraid==2]
		logphi = polynomial(redshift,p,1)
		p=parameters[paraid==3]	
		Lbreak = doublepower(redshift,(p[0],zref, p[1], p[2]))
		parameters_at_z = np.array([gamma1,gamma2,logphi,Lbreak])
	elif model=="Shallowfaint":
		parameters=pglobal_shallowfaint
                zref = 2.
                p=parameters[paraid_shallowfaint==0]
                gamma1 = powerlaw_gamma1(redshift,(p[0],zref,p[1]))
                p=parameters[paraid_shallowfaint==1]
                gamma2 = doublepower(redshift,(p[0],zref,p[1],p[2]))
                p=parameters[paraid_shallowfaint==2]
                logphi = polynomial(redshift,p,1)
                p=parameters[paraid_shallowfaint==3]
                Lbreak = doublepower(redshift,(p[0],zref,p[1],p[2]))
		parameters_at_z = np.array([gamma1,gamma2,logphi,Lbreak])

	return get_model_lf(parameters_at_z,nu,redshift,dtg,convolver)

def cumulative_emissivity(L_nu,Phi_nu,L_limit_low,L_limit_up,nu):
	def logphi(x):
		if (x>=np.min(L_nu)) and (x<=np.max(L_nu)):
			return np.interp(x, L_nu, Phi_nu)
		elif (x<np.min(L_nu)):
			return Phi_nu[0] + (x-L_nu[0])*(Phi_nu[1]-Phi_nu[0])/(L_nu[1]-L_nu[0])
		elif (x>np.max(L_nu)):
			return Phi_nu[-1] + (x-L_nu[-1])*(Phi_nu[-1]-Phi_nu[-2])/(L_nu[-1]-L_nu[-2])

	def emis(x):
		return np.power(10.,logphi(x))*np.power(10.,x)/nu

	result = quad(emis, L_limit_low, L_limit_up)[0]*np.power(10.,L_solar)
	#result = romberg(emis, L_limit_low, L_limit_up, divmax=20)*np.power(10.,L_solar)
	#if np.isfinite(result) == False:
	#	print L_nu
	#	print Phi_nu

	return result

def to_be_integrate(z, nuobs, convolver, model="Fiducial"):
	dtg = return_dtg(z)
	nuem = nuobs*(1+z)
        L_nu, PHI_nu = get_model_lf_global(nuem, z, dtg, convolver, model=model)
        emissivity = cumulative_emissivity(L_nu, PHI_nu, L_nu[0], L_nu[-1], nuem) 
	return emissivity/4./np.pi/(cosmo.luminosity_distance(z).value*1e6*con.pc.value*1e2)**2  * cosmo.differential_comoving_volume(z).value 

import matplotlib.pyplot as plt 
import matplotlib

matplotlib.style.use('classic')
matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

fig=plt.figure(figsize = (15,10))
ax = fig.add_axes([0.13,0.12,0.79,0.83])

E_list = np.logspace(-1,3,100) #100
nu_list = E_list*1000.*con.e.value/con.h.value

zbins = np.linspace(0,7,50) 
zcenters = (zbins[1:]+zbins[:-1])/2.
deltaz= zbins[5]-zbins[4]

Intensity = np.zeros((len(E_list),4))
#Intensity_shallowfaint = np.zeros(len(E_list))
unit_convertion = 1e-7/(1000.*con.e.value) #erg to keV
'''
for i in range(len(E_list)):
	for k in range(len(zcenters)):
		Intensity_shallowfaint[i] += to_be_integrate(zcenters[k],nu_list[i],convolver_list[0],model="Shallowfaint")*deltaz
	print i
'''
for i in range(len(E_list)):
	for j in range(len(convolver_list)):
		Intensity[i,j]=0
		for k in range(len(zcenters)):
			Intensity[i,j] += to_be_integrate(zcenters[k],nu_list[i],convolver_list[j])*deltaz
		#Intensity[i]=quad( to_be_integrate, 0, 7, args=(nu_list[i]) )[0]
	print i


ax.plot(E_list, nu_list * Intensity[:,0] * unit_convertion, c='royalblue', label=r'$\rm This$ $\rm work:$ $\rm AGN$')
#print nu_list * Intensity * unit_convertion
#ax.plot(E_list, nu_list * Intensity_shallowfaint[:] * unit_convertion, c='yellow')

ax.plot(E_list, nu_list * Intensity[:,0] * unit_convertion + 2., c='black', label=r'$\rm AGN$ + $\rm galaxies$')

ax.plot(E_list, nu_list * Intensity[:,1] * unit_convertion, '--', dashes=(25,15), c='magenta', label=r'$\rm AGN$ ($\rm CTK$)')

ax.plot(E_list, nu_list * Intensity[:,2] * unit_convertion, '--', dashes=(25,15), c='navy', label=r'$\rm AGN$ ($\rm CTN,\,absorbed$)')

ax.plot(E_list, nu_list * Intensity[:,3] * unit_convertion, '--', dashes=(25,15), c='deepskyblue', label=r'$\rm AGN$ ($\rm CTN,\,unabsorbed$)')

data=np.genfromtxt("Hopkins2007.dat",names=["x","y"])
ax.plot(data["x"],data["y"]+2.,'--',dashes=(25,15),c='crimson',label=r'$\rm Hopkins+$ $\rm 2007$')

data=np.genfromtxt("ajello2008.dat",names=True)
ax.errorbar(data['E'],data['CXB'],xerr=data['dE'],yerr=data['dCXB'],c='gray',mec='gray',capsize=0,capthick=0,linestyle='none',marker='.',lw=2,ms=1,label=r'$\rm Ajello+$ $\rm 2008$')

data=np.genfromtxt("churazov2007.dat",names=True)
ax.errorbar(data['E'],data['CXB'],xerr=(data['E']-data['Elo'],data['Eup']-data['E']),yerr=(data['CXB']-data['lo'],data['up']-data['CXB']),c='seagreen',mec='seagreen',capsize=0,capthick=0,linestyle='none',marker='.',lw=2,ms=1,label=r'$\rm Churazov+$ $\rm 2007$')

data=np.genfromtxt("Cappelluti2017.dat",names=True)
ax.errorbar(data['E'],data['CXB'],yerr=(data['CXB']-data['lo'],data['up']-data['CXB']),c='cyan',mec='cyan',capsize=0,capthick=0,linestyle='none',marker='.',lw=2,ms=1,label=r'$\rm Cappelluti+$ $\rm 2017$')

data=np.genfromtxt("Moretti2009.dat",names=True)
ax.errorbar(data['E'],data['CXB'],yerr=(data['CXB']-data['lo'],data['up']-data['CXB']),c='pink',mec='pink',capsize=0,capthick=0,linestyle='none',marker='.',lw=2,ms=1,label=r'$\rm Moretti+$ $\rm 2009$')

data=np.genfromtxt("Gruber1999.dat",names=True)
ax.errorbar(data['E'],data['CXB'],xerr=(data['E']-data['left'],data['right']-data['E']),yerr=(data['CXB']-data['lo'],data['up']-data['CXB']),c='chocolate',mec='chocolate',capsize=0,capthick=0,linestyle='none',marker='.',lw=2,ms=1,label=r'$\rm Gruber+$ $\rm 1999$')

data=np.genfromtxt("gendreau1995.dat",names=True)
ax.errorbar(data['E'],data['CXB'],yerr=(data['CXB']-data['lo'],data['up']-data['CXB']),c='darkorchid',mec='darkorchid',capsize=0,capthick=0,linestyle='none',marker='.',lw=2,ms=1,label=r'$\rm Gendreau+$ $\rm 1995$')

prop = matplotlib.font_manager.FontProperties(size=21.5)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.3,loc=2,ncol=3,columnspacing=0.2,frameon=False)
ax.set_xlabel(r'$\rm E$ [$\rm keV$]',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\nu I_{\rm XRB,\nu}$ [$\rm keV^{2}\,s^{-1}\,cm^{-2}\,sr^{-1}\,keV^{-1}$]',fontsize=40,labelpad=5)

#ax.text(0.25, 0.64, r'$\rm <-21$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,color='gray')

ax.set_xlim(0.3,400)
ax.set_ylim(1.,180.)
ax.set_yscale('log')
ax.set_xscale('log')
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
plt.savefig("../figs/CXB_new.pdf",fmt='pdf')
#plt.show()
