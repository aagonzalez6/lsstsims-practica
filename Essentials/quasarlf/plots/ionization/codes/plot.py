from data import *
import numpy as np 
from lf_shape import *
from new_load_kk18_lf_shape import *
import scipy.interpolate as inter
from scipy.integrate import quad
from scipy.integrate import romberg
from convolve import *
from convolve_h07 import *
from scipy.optimize import curve_fit
from scipy.optimize import minimize
from scipy.optimize import least_squares
import astropy.constants as con
# fit the luminosity function based on datasets at a given redshift
from ctypes import *
import ctypes
import sys

# local source approximation used in H07
def Gamma_old(epsilon,z):
	alphaEUV=-1.76
	return ( 2.*(1+z)**(-1.5)*epsilon/1e24/(3.+np.abs(alphaEUV)) )

# our emissivity model
def emis_model(x,eps0,a,b,c,d):
        return eps0 + np.log10( (1+x)**a * np.exp(-b*x)/(np.exp(c*x)+d))

# our local source approximation
def Gamma(epsilon,z):
        alphaEUV = -1.70 + 1.5 #hardening due to IGM filtering
        eta = 2.5+1.94
        Dl = 50.
        return 0.46 * (Dl/50.) * np.power((1.+z)/4.5,3.-eta) * epsilon/1e24 / (3.+np.abs(alphaEUV))
	
	#data = np.genfromtxt("FG19_mfp.dat")
	#Dl = inter.interp1d(data[:,0],data[:,1])	
	#corr = (1.-1./np.e)
	#epsilon = 10**emis_model(z,24.065, 5.941, -0.763, 3.085, 14.571)
	#return 0.46 * (Dl(z)/50.) * np.power((1.+z)/4.5,3.) * epsilon/1e24 / (3.+np.abs(alphaEUV))

parameters_init = np.array([0.41698725, 2.17443860, -4.82506430, 13.03575300, 0.63150872, -11.76356000, -14.24983300, -0.62298947, 1.45993930, -0.79280099])

data=np.genfromtxt("../../../codes/lf_fit/output/fit_at_z_fix.dat",names=True)
pgamma1_fix, pgamma1_err_fix  = data["gamma1"], data["err1"]
pgamma2_fix, pgamma2_err_fix  = data["gamma2"], data["err2"]
plogphis_fix,plogphis_err_fix = data["phi_s"],  data["err3"]
pLbreak_fix, pLbreak_err_fix  = data["L_s"],    data["err3"]
pz_fix=data['z']
zpoints_fix=np.array(pz_fix)

data=np.genfromtxt("../../../codes/lf_fit/output/fit_at_z_nofix.dat",names=True)
pgamma1_free, pgamma1_err_free  = data["gamma1"], data["err1"]
pgamma2_free, pgamma2_err_free  = data["gamma2"], data["err2"]
plogphis_free,plogphis_err_free = data["phi_s"],  data["err3"]
pLbreak_free, pLbreak_err_free  = data["L_s"],    data["err3"]
pz_free=data['z']
zpoints_free=np.array(pz_free)

data=np.genfromtxt("../../../codes/lf_fit/output/special_fit.dat",names=True)
pgamma1_spe, pgamma1_err_spe  = data["gamma1"], data["err1"]
pgamma2_spe, pgamma2_err_spe  = data["gamma2"], data["err2"]
plogphis_spe,plogphis_err_spe = data["phi_s"],  data["err3"]
pLbreak_spe, pLbreak_err_spe  = data["L_s"],    data["err3"]
pz_spe=data['z']
zpoints_spe=np.array(pz_spe)

fit_evolve=np.genfromtxt("../../Fit_parameters/codes/zevolution_fit_global.dat",names=True)
paraid, pglobal, pglobal_err = fit_evolve['paraid'], fit_evolve['value'], (fit_evolve['uperr']+fit_evolve['loerr'])/2.
zlist=np.linspace(0.1,7,30)

zpoints_free = zpoints_free[zpoints_free>2.]
zpoints_fix  = zpoints_fix[zpoints_fix>2.]
zlist = zlist[zlist>2.]

#load the shared object file
c_extenstion = CDLL(homepath+'codes/c_lib/convolve.so')
convolve_c = c_extenstion.convolve
convolve_c.restype = ctypes.POINTER(ctypes.c_double * N_bol_grid)

#old convolution code for H07
c_extenstion_old = CDLL(homepath+'codes/c_lib/convolve_old.so')
convolve_c_old = c_extenstion_old.convolve
convolve_c_old.restype = ctypes.POINTER(ctypes.c_double * N_bol_grid)

def get_model_lf_global(parameters,nu,redshift,magnitude=False):
	zref = 2.
	p=parameters[paraid==0]
	gamma1 = polynomial(redshift,p,2)
	p=parameters[paraid==1]
	gamma2 = doublepower(redshift,(p[0],zref,p[1],p[2]))
	p=parameters[paraid==2]
	logphi = polynomial(redshift,p,1) 
	p=parameters[paraid==3]	
	Lbreak = doublepower(redshift,(p[0],zref,p[1],p[2]))
	parameters_at_z = np.array([gamma1,gamma2,logphi,Lbreak])
	return get_model_lf(parameters_at_z,nu,redshift,magnitude=magnitude)

def get_model_lf(parameters,nu,redshift,magnitude=False):
        L_band = bolometric_correction(L_bol_grid,nu)
        nu_c = c_double(nu)
	redshift_c = c_double(redshift)
	dtg_c = c_double(return_dtg(redshift))
        input_c= np.power(10.,LF(L_bol_grid,parameters)).ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        res = convolve_c(input_c,nu_c,redshift_c,dtg_c)
        res = [i for i in res.contents]
        PHI_band = np.array(res,dtype=np.float64)
        if magnitude==False:
                return L_band, np.log10(PHI_band)
        else:
		M_1450 = -2.5*(L_band+L_solar-np.log10(Fab*(con.c.value/1450e-10)))
                PHI_1450 = np.log10(PHI_band) - np.log10(2.5)
                return M_1450, PHI_1450

def cumulative_emissivity(L_band,Phi_band,L_limit_low,L_limit_up):
	Mband, Mlow, Mup = L_band, L_limit_low, L_limit_up
	logphi=inter.interp1d(Mband,Phi_band)
	def emis(x):
		fnu = np.power(10.,-0.4*x)*3631*1e-23*4*np.pi*(10*con.pc.value*100)**2
		fnu912 = fnu * (912./1450.)**(0.61)
		return np.power(10.,logphi(x))*fnu912
	return romberg(emis,Mlow,Mup,divmax=20)
	#return quad(emis,Mlow,Mup)[0]

def Gamma_err(parameters,errs,L_limit_low,L_limit_up,redshift,global_fit=False):
	partials = 0.0 * parameters
	delta = 1e-6
	if global_fit==False:
		def fobjective(parameters):
			M_1450, PHI_1450 = get_model_lf(parameters, -5, redshift, magnitude=True)
        		return Gamma(cumulative_emissivity(M_1450, PHI_1450, L_limit_low, L_limit_up),redshift)
		for i in range(len(parameters)):
			parameters_add = parameters.copy()
			parameters_add[i] += delta
			partials[i] = ( fobjective(parameters_add) - fobjective(parameters))/delta
			partials[i] = np.abs(partials[i])
	else:
		def fobjective(parameters):
                        M_1450, PHI_1450 = get_model_lf_global(parameters, -5, redshift, magnitude=True)
                        return Gamma(cumulative_emissivity(M_1450, PHI_1450, L_limit_low, L_limit_up),redshift)
                for i in range(len(parameters)):
                        parameters_add = parameters.copy()
                        parameters_add[i] += delta
                        partials[i] = ( fobjective(parameters_add) - fobjective(parameters))/delta
                        partials[i] = np.abs(partials[i])

	return np.sqrt(np.sum((partials * errs)**2))

import matplotlib.pyplot as plt 
import matplotlib

matplotlib.style.use('classic')
matplotlib.rc('xtick.major', size=15, width=3)
matplotlib.rc('xtick.minor', size=7.5, width=3)
matplotlib.rc('ytick.major', size=15, width=3)
matplotlib.rc('ytick.minor', size=7.5, width=3)
matplotlib.rc('lines',linewidth=4)
matplotlib.rc('axes', linewidth=4)

fig=plt.figure(figsize = (15,10))
ax = fig.add_axes([0.13,0.12,0.79,0.83])

lowlimit=-35

zlist_h07=np.linspace(0.1,7,20)
result=np.zeros((len(zlist_h07),2))
for i in range(len(zlist_h07)):
	L_band = bolometric_correction_old(L_bol_grid,-1)
        nu_c = c_double(-1)
        input_c= np.power(10.,LF_at_z_H07(L_bol_grid,parameters_init,zlist_h07[i],"Fiducial")).ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        res = convolve_c_old(input_c,nu_c)
        res = [j for j in res.contents]
        PHI_band = np.array(res,dtype=np.float64)
	M_1450 = (M_sun_Bband_AB -2.5*L_band) + 0.706
	PHI_1450 = np.log10(PHI_band) - np.log10(2.5)

	result[i,0]= Gamma_old(cumulative_emissivity(M_1450, PHI_1450, lowlimit, -18),zlist_h07[i])
	#result[i,1]= Gamma_old(cumulative_emissivity(M_1450, PHI_1450, lowlimit, -21),zlist_h07[i])
ax.plot(zlist_h07,np.log10(result[:,0]),'--',dashes=(25,15),c='crimson',label=r'$\rm Hopkins+$ $\rm 2007$')

result=np.zeros((len(zlist),2))
uncertainty=np.zeros((len(zlist),2))
for i in range(len(zlist)):
	M_1450, PHI_1450 = get_model_lf_global(pglobal, -5, zlist[i], magnitude=True)
	result[i,0]= Gamma(cumulative_emissivity(M_1450, PHI_1450, lowlimit, -18),zlist[i])
	#result[i,1]= Gamma(cumulative_emissivity(M_1450, PHI_1450, lowlimit, -21),zlist[i])
	uncertainty[i,0]= Gamma_err(pglobal, pglobal_err, lowlimit, -18, zlist[i], global_fit=True)
uperr = np.log10(result + uncertainty)-np.log10(result)
loerr = np.log10(result)-np.log10(result - uncertainty)
loerr[np.invert(np.isfinite(loerr))] = 100
ax.plot(zlist,np.log10(result[:,0]),'-',c='darkorchid',alpha=0.7,label=r'$\rm Global$ $\rm fit$ ($\rm local$ $\rm approx.$)')
ax.fill_between(zlist, y1=np.log10(result[:,0])+uperr[:,0] ,y2=np.log10(result[:,0])-loerr[:,0], color='darkorchid', alpha=0.4)

data=np.genfromtxt("FG20_qso.dat")
ax.plot(10**data[:,0]-1,np.log10(data[:,1]/1e-12), color='navy',label=r'$\rm Global$ $\rm fit$ ($\rm full$ $\rm UVB$ $\rm calc.$)')

###### fit at a given redshift
result=np.zeros((len(zpoints_free),2))
uncertainty=np.zeros((len(zpoints_free),2))
for i in range(len(zpoints_free)):
        id = pz_free==zpoints_free[i]
        M_1450, PHI_1450 = get_model_lf([pgamma1_free[id],pgamma2_free[id],plogphis_free[id],pLbreak_free[id]], -5, zpoints_free[i], magnitude=True)
        result[i,0]= Gamma(cumulative_emissivity(M_1450, PHI_1450, lowlimit, -18),zpoints_free[i])
        uncertainty[i,0]= Gamma_err(np.array([pgamma1_free[id],pgamma2_free[id],plogphis_free[id],pLbreak_free[id]]), [pgamma1_err_free[id],pgamma2_err_free[id],plogphis_err_free[id],pLbreak_err_free[id]], lowlimit, -18, zpoints_free[i])
        #result[i,1]= Gamma(cumulative_emissivity(M_1450, PHI_1450, lowlimit, -21),zpoints_free[i])
uperr = np.log10(result + uncertainty)-np.log10(result)
loerr = np.log10(result)-np.log10(result - uncertainty)
loerr[np.invert(np.isfinite(loerr))] = 100
uperr[np.invert(np.isfinite(uperr))] = 100
ax.errorbar(zpoints_free,np.log10(result[:,0]),yerr=(loerr[:,0],uperr[:,0]),linestyle='none',lw=2,marker='x',c='gray',mec='gray',ms=15,capsize=9,capthick=2,alpha=0.7)


result=np.zeros((len(zpoints_fix),2))
uncertainty=np.zeros((len(zpoints_fix),2))
for i in range(len(zpoints_fix)):
        id= pz_fix==zpoints_fix[i]
        M_1450, PHI_1450 = get_model_lf([pgamma1_fix[id],pgamma2_fix[id],plogphis_fix[id],pLbreak_fix[id]], -5, zpoints_fix[i], magnitude=True)
        result[i,0]= Gamma(cumulative_emissivity(M_1450, PHI_1450, lowlimit, -18),zpoints_fix[i])
	#result[i,1]= Gamma(cumulative_emissivity(M_1450, PHI_1450, lowlimit, -21),zpoints_fix[i])
ax.plot(zpoints_fix,np.log10(result[:,0]),linestyle='none',marker='x',c='royalblue',mec='royalblue',ms=15)

data=np.genfromtxt("kk18.dat",names=['z','gamma'])
ax.plot(data['z'],data['gamma'],'--',dashes=(25,15),c='seagreen',label=r'$\rm Kulkarni+$ $\rm 2018$')
'''
result=np.zeros((len(zpoints_spe),2))
uncertainty=np.zeros((len(zpoints_spe),2))
for i in range(len(zpoints_spe)):
	id= pz_spe==zpoints_spe[i]
	M_1450, PHI_1450 = get_model_lf([pgamma1_spe[id],pgamma2_spe[id],plogphis_spe[id],pLbreak_spe[id]], -5, zpoints_spe[id], magnitude=True)
	result[i,0] = Gamma(cumulative_emissivity(M_1450, PHI_1450, lowlimit, -18),zpoints_spe[id])
ax.plot(zpoints_spe,np.log10(result[:,0]),linestyle='none',lw=2,marker='v',c='gold',mec='gold',ms=15,label=r'$\rm Special$ $\rm fit$')
'''
#######################################################################
'''
data=np.genfromtxt("Kuhlen2012.dat",names=True)
ax.errorbar(data["z"], data["gamma"] ,yerr=(data["gamma"]-data["lo"],data["up"]-data["gamma"]),marker='o',linestyle='none',ms=15,color='k',mec='k',capsize=0,label=r'$\rm Kuhlen+$ $\rm 2012$')
'''

data=np.genfromtxt("Wyithe2011.dat",names=True)
ax.errorbar(data['z'],data['gamma'], yerr=(data["gamma"]-data["lo"],data["up"]-data["gamma"]), marker='o',linestyle='none',ms=15,color='olive',mec='olive',label=r'$\rm Wyithe+$ $\rm 2011$')

data=np.genfromtxt("Calverley2011.dat",names=True)
ax.errorbar(data['z'],data['gamma'], yerr=(data["gamma"]-data["lo"],data["up"]-data["gamma"]), marker='o',linestyle='none',ms=15,color='chocolate',mec='chocolate',label=r'$\rm Calverley+$ $\rm 2011$')

ax.errorbar([2.40,2.80,3.20,3.60,4.00,4.40,4.75],[0.015,-0.066,-0.103,-0.097,-0.072,-0.019,-0.029],yerr=([0.146, 0.131, 0.121, 0.118, 0.117, 0.122, 0.147],[0.132, 0.129, 0.130, 0.131, 0.135, 0.140, 0.156]),marker='o',linestyle='none',ms=15,color='k',mec='k',capsize=0,label=r'$\rm Becker$ $\rm &$ $\rm Bolton+$ $\rm 2013$')

data=np.genfromtxt("Gaikwad2017.dat",names=['z','gamma'])
ax.plot(data['z'],data['gamma'], marker='o',linestyle='none',ms=15,color='deeppink',mec='deeppink',label=r'$\rm Gaikwad+$ $\rm 2017$')

ydata=np.array([0.58, 0.53, 0.48, 0.47, 0.45, 0.29])
lowerr=np.array([0.20, 0.19, 0.18, 0.18, 0.17, 0.11])
uperr=np.array([0.08, 0.09, 0.10, 0.12, 0.14, 0.11])
ax.errorbar([4.8,5.0,5.2,5.4,5.6,5.8], np.log10(ydata) ,yerr=(np.log10(ydata)-np.log10(ydata-lowerr),np.log10(ydata+uperr)-np.log10(ydata)),marker='o',linestyle='none',ms=15,color='gray',mec='gray',capsize=0,label=r'$\rm Aloisio+$ $\rm 2018$')

#######################################################################

prop = matplotlib.font_manager.FontProperties(size=21.5)
ax.legend(prop=prop,numpoints=1, borderaxespad=0.3,loc=3,ncol=2,columnspacing=0.2,frameon=False)
ax.set_xlabel(r'$\rm z$',fontsize=40,labelpad=2.5)
ax.set_ylabel(r'$\log{(\Gamma_{\rm -12}\,[\rm s^{-1}\,atom^{-1}])}$',fontsize=40,labelpad=5)

#ax.text(0.25, 0.64, r'$\rm <-21$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,color='gray')
#ax.text(0.25, 0.87, r'$\rm <-18$' ,horizontalalignment='center',verticalalignment='center',transform=ax.transAxes,fontsize=30,color='gray')

ax.set_xlim(0.1,7)
ax.set_ylim(-2.5,0.4)
ax.tick_params(labelsize=30)
ax.tick_params(axis='x', pad=7.5)
ax.tick_params(axis='y', pad=2.5)
ax.minorticks_on()
plt.savefig("../figs/ionizing_photon.pdf",fmt='pdf')
#plt.show()

