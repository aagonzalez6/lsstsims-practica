import sys
import argparse
import collections
from bisect import bisect_left
import time
import pickle
import datetime
import smtplib

import numpy as np
from colossus.cosmology import cosmology

from Integral_extension_V5 import NtotSimps ##### OJO CON DSP ESCRIBIR QUE VERSION SE OCUPA, AL FINAL, AL GUARDAR

# ================================== Argparse =====================================
def parseRango(string):
	tupla_rango = [float(x) for x in string.split("-")]
	if len(tupla_rango) != 2:
		raise argparse.ArgumentTypeError("debe ser de la forma zi-zf")
	return tupla_rango 

parser = argparse.ArgumentParser()

#parser.add_argument("--zrango","-z",type=parseRango,help = "redshift inicial, zi-zf")
parser.add_argument("--filtro","-f",help = "filtro a usar, u/g/r/i/z/y")
parser.add_argument("--modelo","-m",help = "modelo usado, A o B, defecto A")
args = parser.parse_args()
if not args.filtro:
	raise argparse.ArgumentTypeError("se deben definir filtro, vease -h")
if not args.modelo:
	modelo_usado = "A"
else:
	modelo_usado = args.modelo

filtro_a_usar = args.filtro


# ==========================================================================================
# ===================================== COLOSSUS ===========================================
# ==========================================================================================
sigma8 = 0.82
ns = 0.96
params = {'flat': True, 'H0': 70, 'Om0': 0.30, 'Ob0': 0.049, 'sigma8': 0.81, 'ns': 0.95,"relspecies":False}
h = params["H0"]/100
colossus_cosmo = cosmology.setCosmology('myCosmo',params)

# ==========================================================================================
# ==========================================================================================
# ==========================================================================================
# ==================================== Def run ===================================
step_def = 0.01
mlim_min = 20
mlim_max = 30
rango_def = np.arange(mlim_min,mlim_max+step_def,step_def)
print2 = "len(rango_def_mlim_{}) ={} ".format(filtro_a_usar,len(rango_def))
print(print2)


print3 = "filtro {0}     modelo: {1}".format(filtro_a_usar,modelo_usado)
print(print3)
diccionario = {mlim:NtotSimps(colossus_cosmo,mlim,1,filtro_a_usar,modelo = modelo_usado) for mlim in rango_def} # el 1 porque despues se multiplica por area radianes
 # 0.274 sqrad = 900deg^2


# ================================ Write to file ==================================
version_integral = "V5_" # si es version 4 dejar en blanco : ""
nombre_archivo = "./LookupTables/{0}{1}_mod{2}_LookupT_extension.pkl".format(version_integral,filtro_a_usar,modelo_usado)
a_file = open(nombre_archivo, "wb")
pickle.dump(diccionario, a_file)
a_file.close()
print4 = "Done"
print(print4)


