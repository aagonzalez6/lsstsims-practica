import time
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad,simps
from colossus.cosmology import cosmology
from quasarlf.pubtools.utilities import *
import astropy.constants as con
#start = time.time()

def srad(sqdeg): # de sqdeg a srad
	return (np.pi/180)**2 * sqdeg

def lam_to_nu(lambdita_ang):
	c_angstroms = con.c.value/1E-10
	return c_angstroms/lambdita_ang


class cronometro():
	def __init__(self):
		self.times = [0,0]

	def clicki(self):
		print("__________________________________")
		self.times[0] = time.perf_counter()
	def clickf(self):
		self.times[1] = time.perf_counter()
		penultimo_click = self.times[-2]
		ultimo_click = self.times[-1]
		print("dt 2 ultimos clicks:",ultimo_click-penultimo_click)
		print("     ^^^^^^^^^^^^^^^^^^^^^^^     \n")


c = 300000 #km/s
# ========================= Modelo SHELLQS ================================

H0 = 70 #km/s/Mpc      #70/10**6 #km/s/pc
OmegaLamb = 0.7

############ ________________ 5.7 < z < 6.5 _________________________
k = -0.7
Phi_st = 10.9 # Gpc^-3 mag^-1      #8.1/10**27  # pc^-3 mag^-1
M_st = -24.9 # Errores : (-0.9, +0.75)
alpha = -1.23 # Errores : (-0.34, 0.44)
beta = -2.73 # Errores: (-0.31, 0.23)
# ==========================================================================
def colos_dVdz(colossus_cosmo,z,omega,printmode=None):
	colos_DL = colossus_cosmo.luminosityDistance(z)/h # Mpc
	colos_diffDL = colossus_cosmo.luminosityDistance(z,1)/h # Mpc/z?
	dVdz = (colos_DL/(1+z))**2*((1+z)**-1*colos_diffDL- colos_DL*(1+z)**-2)
	if printmode == "scientific":
		print("Colossus dVdz: {:.2E}".format(dVdz))
	elif printmode == "extended":
		print("Colossus dVdz: {}".format(dVdz))
	return dVdz*omega


def M_lim(colossus_cosmo,z,mlim):
	mu = colossus_cosmo.distanceModulus(z)
	#etta = 2.5/2*np.log10(1450/9749*(1+z)) #9749 = mean wavelenght of Y filter # esto era cuando no coincidia band con magn observada
	resultado = mlim - mu # - etta
	return resultado

def integral_interior(colossus_cosmo,z,mlim,filtro_LSST,modelo):
	M_limite = M_lim(colossus_cosmo,z,mlim)
	""" La lista que me entrega return_qlf_in_band probablemente no posee este M_limite en sus valores de M, asi que
	se interpola este valor phi(M_limite) """
	dict_filtros = {"u":3694,"g":4841,"r":6258,"i":7560,"z":8701,"y":9749} #lambda mean de filtros
	lambda_rf_filtro = dict_filtros[filtro_LSST]
	lambda_filtro_realRF = lambda_rf_filtro/(z+1) # esta lambda del rf del espectro coincide con el lambda del  filtro observado
	rangoM,Phi = return_qlf_in_band(z, lam_to_nu(lambda_filtro_realRF), model=modelo, version="Ar") #z= 2 , nu = -5 := M1450 # mag, n/mag
	Phi = 10**(Phi) #pasa de log10[mag-1Mpc-3] a [mag-1Mpc-3]
	"""habria que hacer aqui un if si es que se diera milagorsamente el caso que M_lim es justo un valor presente en
	return_qlf_in_band """
	Phi_M_limite = np.interp(M_limite,rangoM,Phi)
	M_menor_aMlim = np.array([x for x in rangoM if x < M_limite])
	Phi_correspondiente = Phi[-len(M_menor_aMlim):]
	# el rangoM que entrega return_qlf_in_band esta al reves e.g. -11, -11.5, -12 ...#
	#print(M_menor_aMlim)
	M_menorigual_aMlim = np.insert(M_menor_aMlim, 0, M_limite, axis=0)
	Phi_correspondiente = np.insert(Phi_correspondiente,0,Phi_M_limite, axis=0 )
	return simps(Phi_correspondiente,M_menorigual_aMlim)

#aqui el rango M no es de -200 como antes sino que de -37 masomenos que es casi lo mismo, porque ahi la LF es despreciable
#(esto a partir mas o menos de M= -30)

def NtotSimps(colossus_cosmo,mlim,omega,filtro_LSST,extdz=1/1000,modelo = "A"): #VER ESTOOOOOOOO PORQUE ANTES EL deltaz1 = z/1000 !!!!!!!!!!!!!
	
	rangosZfiltros = {"u":[1.5,2.2],"g":[2.2,3.3],"r":[3.3,4.5],"i":[4.5,5.6],"z":[5.6,6.4],"y":[6.4,7.3]}
	z_i, z_f = rangosZfiltros[filtro_LSST] 

	#my = 24 # limiting magnitude in Y band of lsst# esto era para probar en etapas inicialesz

	rango_z = np.arange(z_i,z_f, extdz) #de z =6 a z=14 en vez de inf xd

	""" Para cada z en el rango_z necesito el valor de la integral interior, voy a tener una := lista_exterior. Luego
	se debe usar simps o cumtrapz a esta lista_exterior
	Cada integral interior viene dada por usar simps o cumtrapz en la lista retornada por return_qlf_in_band
	previamente recortada hasta la M_lim """


	lista_exterior = [integral_interior(colossus_cosmo,z,mlim,filtro_LSST,modelo = modelo)*colos_dVdz(colossus_cosmo,z,omega) for z in rango_z]

	return -simps(lista_exterior, rango_z) # el - porque las magnitudes venian al reves entonces al integrar sale un menos


