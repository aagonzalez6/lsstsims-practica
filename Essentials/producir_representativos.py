import pickle
import os
import fnmatch

import numpy as np

def loghist_representativo(lista_hist_pixel,opc = "media"):
	data_util = lista_hist_pixel[lista_hist_pixel != None] # data que si tiene pares de visitas ie un pixel que posee un histograma
	bines = data_util[0][1]
	if opc == "media":
		y_hist = np.mean([pixel_hist[0] for pixel_hist in data_util], axis=0)
	elif opc == "mediana":
		y_hist = np.median([pixel_hist[0] for pixel_hist in data_util], axis=0)

	
	return [y_hist, bines] # loghist

#hacer loop pa guardar toods los representativos en un solo archivo por FBS



# Ej nombre : Npair_data_FBS1.5_z_WFD@spiders_v1.5_10yrs.pkl
dir_entrada = "./NpairSavedResults/" 
dir_salida = "./NpairRepresentativeResults/"
for filename in os.listdir(dir_entrada): # poner directorio de archivos Npair data
	if fnmatch.fnmatch(filename,"Npair*.pkl") :
		# parametros,runname = filename[-4:].split("@")
		# *_, FBS, filtro, WFD = parametros.split("_")
		a_file = open(dir_entrada + filename, "rb")
		lista_hist_pixel = pickle.load(a_file)
		a_file.close()
		
		estadistico = "mediana"
		rep_loghist = loghist_representativo(lista_hist_pixel,opc = estadistico)
		
		dict_prefijos = {"media":"MDA","mediana":"MDN"}
		b_file = open(dir_salida+dict_prefijos[estadistico]+"_repLoghist"+filename[10:], "wb")
		pickle.dump(rep_loghist, b_file)
		b_file.close()


