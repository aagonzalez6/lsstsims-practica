En estos archivos se encuentran las tablas con la totalidad de resultados obtenidos. En cuanto a la métrica Ntot, su nomenclatura es simple y sigue las líneas descritas en el informe. 

-  *zy*: Detecciones en bandas z/y (correspondiente al rango de redshifts 5.6-7.3)  
-  *izzy*: Identificaciones en bandas z/y (correspondiente al rango de redshifts 5.6-7.3)  
-  *ugrizy*: Detecciones en todas las bandas (correspondiente al rando de redhisfts 1.5-7.3)  
