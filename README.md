# LSSTsims-practica

Proyecto para práctica de Licenciatura de Ariel Gonzalez Carrasco. Los codigos aquí usados fueron usados bajo el entorno de trabajo LSST Science Pipelines, específicamente el paquete `lsst_sims` versión w_2020_06. Véase [Catalogs and MAF](https://confluence.lsstcorp.org/display/SIM/Catalogs+and+MAF "Pasos instalacion LSST Simulation Framework"). Adicionalmente, se usó el paquete [Colossus](https://bdiemer.bitbucket.io/colossus/ "A Python Toolkit for Cosmology, Large-scale Structure, and Dark Matter Halos").
### Ver Histogramas R
Si sólo se quiere ver los resultados obtenidos, basta con visitar la carpeta Histogramas R y seleccionar el FBS/zona/filtro deseado. Los resultados poseen antepuesto un numero que expresa su ranking según el parámetro AR (menos es mejor).

_______
Para correr las métricas es necesario poseer las simulaciones. Estas son fácilmente descargables mediante los bash scripts dentro de la carpeta lsst_cadence.
##### Ntot metric
El orden de ejecución y procesamiento de datos para esta métrica es como sigue:

1. Crear LookupTables, para ello se debe ocupar el script `./Essentials/Make_lookup_table.py` especificando el filtro de la LF con `-f filtro`.
2. Correr métrica usando el archivo EnMasa_bolNTOT-all* de interés. Al interior del archivo se debe especificar el directorio de salida(`outDir` y `metricDataPath`) de los resultados de la métrica. Se debe entonces correr desde una terminal usando el correspondiente comando `python script.py`
3. Seguidamente, para procesar estos resultados se debe hacer uso de ./Analisis_Resultados/Ntotv2_analisis.py. OBLIGATORIO modificar el `resultDbPath` y `metricDataPath` para que coincidan con `outDir` y `outDir/MetricData` especificados en el paso anterior, respectivamente.
Se puede entonces ejecutar el script. Esto creará archivos pkl que contienen una lista de diccionario cuyas keys son las siguientes: 

|   Otras |     filtro u      |     filtro g      |     filtro r      |     filtro i      |     filtro z      |   filtro y  |
|     ----      |     ----      |     ----      |     ----      |     ----      |     ----      |   ----  |
| Opsim |   u_o5_Ntot   |   g_o5_Ntot   |   r_o5_Ntot   |   i_o5_Ntot    |    z_o5_Ntot   |  y_o10_Ntot   |
| Zona  |   u_o10_Ntot  |   g_o10_Ntot  |  r_o10_Ntot   |   i_o10_Ntot   |    z_o5_Ntot   |  y_o10_Ntot   |
| Area  |               |   ug_o5_Ntot  |  gr_o5_Ntot   |   ri_o5_Ntot   |   iz_o5_Ntot   |  zy_o5_Ntot   |
|       |               |  ug_o10_Ntot  |  gr_o10_Ntot  |   ri_o10_Ntot  |   iz_o10_Ntot  |  zy_o10_Ntot  |



##### Npair metric
Por su parte, esta metrica funciona como sigue:

1. Correr la métrica ejecutando Npair_AllSims_noConstraint.py.
2. Correr `./Analisis_Resultados/CreacionNpairSavedResults.py`,cambiando el dir_salida si se quiere.
3. Correr `producir_representativos.py` verificando los directorios de entra y salida, y el estadistico preferido (media o mediana). Esto creará los histogramas R en su forma RAW.


---------------
El algoritmo de autoselección de histogramas R usado se pone a disposición a través del archivo `algoritmo_AR.py` dentro de la carpeta `Analisis_Resultados`.